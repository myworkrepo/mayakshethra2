from django.contrib import admin
from solo.admin import SingletonModelAdmin
from .models import About, Package, Testimonial, Gallery, Enquiry, \
    ContactDetails, BannerImages, Booking


admin.site.register(About, SingletonModelAdmin)
admin.site.register(BannerImages, admin.ModelAdmin)
admin.site.register(Package, admin.ModelAdmin)
admin.site.register(Testimonial, admin.ModelAdmin)
admin.site.register(Booking, admin.ModelAdmin)
admin.site.register(Gallery, admin.ModelAdmin)
admin.site.register(ContactDetails, SingletonModelAdmin)
admin.site.register(Enquiry, admin.ModelAdmin)
