from django import forms

from .models import Enquiry


class ContactForm(forms.ModelForm):

    class Meta:
        model = Enquiry
        fields = ('name', 'subject', 'email', 'message')
