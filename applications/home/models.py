from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField
from solo.models import SingletonModel


class About(SingletonModel):
    title = models.CharField(max_length=255)
    description = RichTextUploadingField()
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return 'About'

    class Meta:
        verbose_name = "About"


class Package(models.Model):
    title = models.CharField(max_length=255)
    description = RichTextUploadingField()
    image = models.ImageField()
    video = models.URLField(null=True, blank=True)
    price = models.IntegerField()
    is_special = models.BooleanField(default=False)
    place = models.CharField(max_length=255)

    def __str__(self):
        return 'Package'

    class Meta:
        verbose_name = "Package"
        verbose_name_plural = "Packages"


class BannerImages(models.Model):
    image = models.ImageField()
    title = models.CharField(max_length=255)
    sub_title = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return 'Banner Images'

    class Meta:
        verbose_name = "Banner Images"


class ContactDetails(SingletonModel):
    phone = models.CharField(max_length=255)
    email = models.EmailField()
    address = RichTextUploadingField()

    def __str__(self):
        return 'Contact Details'

    class Meta:
        verbose_name = "Contact Details"


class Testimonial(models.Model):
    author = models.CharField(max_length=255)
    testimonial = models.TextField()
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.author

    class Meta:
        verbose_name = "Testimonial"
        verbose_name_plural = "Testimonials"


class Gallery(models.Model):
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = "Gallery"
        verbose_name_plural = "Gallery"


class Enquiry(models.Model):
    name = models.CharField(max_length=235)
    subject = models.CharField(max_length=235)
    email = models.EmailField()
    message = models.TextField()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'Enquiry'
        verbose_name_plural = 'Enquiries'


class Booking(models.Model):
    CHOICES = (
        ('cheap', 'Cheap'),
        ('standard', 'Standard'),
        ('lux', 'Lux'),
    )

    name = models.CharField(max_length=235)
    email = models.EmailField()
    check_in = models.DateField()
    check_out = models.DateField()
    comfort_type = models.CharField(max_length=20, choices=CHOICES, null=True, blank=True)
    no_of_adults = models.IntegerField(default=0)
    no_of_children = models.IntegerField(default=0)
    no_of_rooms = models.IntegerField(default=0)
    message = models.TextField()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'booking'
        verbose_name_plural = 'Bookings'
