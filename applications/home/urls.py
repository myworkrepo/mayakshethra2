#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from applications.home import views


urlpatterns = [

    url(r'^contact/$', (views.ContactView.as_view()), name='contact'),
    url(r'^hot-tours/$', (views.HotTripView.as_view()), name='hot-tours'),
    url(r'^special/$', (views.SpecialOffersView.as_view()), name='special'),
    url(r'^$', (views.HomeView.as_view()), name='home'),
]
