import json

from django.views import generic
from django.http import HttpResponse
from django.http import JsonResponse
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from .forms import ContactForm
from .models import About, Package, Testimonial, Gallery, \
    ContactDetails, BannerImages, Booking


class HomeView(generic.TemplateView):
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['about'] = About.get_solo()
        context['contact'] = ContactDetails.get_solo()
        context['bg_images'] = BannerImages.objects.all()
        context['packages'] = Package.objects.all()[:3]
        context['testimonials'] = Testimonial.objects.all()
        context['galleries'] = Gallery.objects.all()
        return context


class HotTripView(generic.ListView):
    template_name = 'hot_tours.html'
    model = Package
    context_object_name = 'packages'


class SpecialOffersView(generic.ListView):
    template_name = 'special.html'
    model = Package
    queryset = Package.objects.filter(is_special=True)
    context_object_name = 'specials'

    def get_context_data(self, **kwargs):
        context = super(SpecialOffersView, self).get_context_data(**kwargs)
        place = self.request.GET.get('place')
        if place:
            specials = Package.objects.filter(is_special=True, place=place)
            context['specials'] = specials
        context['places'] = set(Package.objects.all().values_list('place', flat=True))

        return context

class ContactView(generic.FormView):

    form_class = ContactForm
    template_name = "contact.html"

    def form_valid(self, form):
        print('dddddddddddddddddddddddddddddddddddddddddddd')
        text_template = get_template('email_templates/contact_message.html')
        user_name = form.cleaned_data['name']
        subject = 'Contact request from {}'.format(user_name)
        context = {'formentry': self.request.POST}
        text_content = text_template.render(context)
        from_email = settings.EMAIL_HOST_USER
        email = ContactDetails.get_solo().email
        to = [email]
        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(text_content, "text/html")
        msg.send()
        form.save()
        return JsonResponse({'status':'success'})

    def form_invalid(self, form):
        print(form.errors)
        return HttpResponse(json.dumps({'result': {'status': 'form_error', 'errors': form.errors}}),
                            content_type='application/json')
